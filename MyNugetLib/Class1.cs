﻿using System;

namespace MyNugetLib
{
    public class Class1
    {
        public string DoSomething()
        {
            var dt = DateTime.Now;
            return dt.ToLongDateString();
        }
    }
}
